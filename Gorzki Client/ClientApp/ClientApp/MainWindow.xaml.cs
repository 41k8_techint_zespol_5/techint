﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        Client client;
        Brush windowBG;
        Brush fullBG;
        private bool isPaused = false;
        private bool _isFullScreen;
        private bool isFullScreen
        {
            set
            {
                _isFullScreen = value;
                setFullScreen(value);
            }
            get
            {
                return _isFullScreen;
            }
        }

        private bool _isSettingsPanelOpened;
        private bool isSettingsPanelOpened
        {
            set
            {
                showSettingsPanel(value);
            }
            get
            {
                return _isSettingsPanelOpened;
            }
        }

        public override void EndInit()
        {
            base.EndInit();
            windowBG = (Brush)(new BrushConverter().ConvertFrom("#333333"));
            fullscreenNewColorPicker.SelectedColor = (Color)ColorConverter.ConvertFromString("Black");
            windowNewColorPicker.SelectedColor = (Color)ColorConverter.ConvertFromString("#333333");
            fullBG = Brushes.Black;
        }

        public MainWindow()
        {
            client = new Client();
            client.readJsonModel();

            DataContext = client;

            InitializeComponent();
            client.setImagesHandler(ref this.contentVideo, ref this.contentImage);

            isFullScreen = client.fullscreen;
            checkboxFullscreen.IsChecked = client.fullscreen;
            optionButtonsPanel.Opacity = 0;
            client.startUp();
        }

        private void setFullScreen(bool goFull)
        {
            if (goFull)
            {
                statusRow.Height = 0;
                this.WindowStyle = WindowStyle.None;
                this.WindowState = WindowState.Maximized;
                this.ResizeMode = ResizeMode.NoResize;
            }
            else
            {
                this.WindowState = WindowState.Normal;
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.ResizeMode = ResizeMode.CanResize;
                statusRow.Height = 60;
            }
            refreshBG();
        }

        private void showSettingsPanel(bool shouldOpen)
        {

            _isSettingsPanelOpened = shouldOpen;
            if (shouldOpen)
            {
                settingsPanel.Visibility = Visibility.Visible;
            }
            else
            {
                settingsPanel.Visibility = Visibility.Hidden;
            }
        }

        //FIRST TIME LOGIN PANEL
        //metoda buttona na pobieranie ręczne
        public void getIDByHand(object sender, RoutedEventArgs e)
        {
            //firstTimeLoginPanel.IsEnabled = false;
            // firstTimeLoginPanel.Visibility = Visibility.Collapsed;
            //to do to gówno do ustawiania ID ręczne
        }

        //metoda buttona na pobieranie z serwera
        public void getIDFromServer(object sender, RoutedEventArgs e)
        {
            // firstTimeLoginPanel.IsEnabled = false;
            //firstTimeLoginPanel.Visibility = Visibility.Collapsed;
            //to do to gówno do ustawiania ID z serwera
        }

        //metoda buttona do resetowania settingsów
        public void resetIDSettings(object sender, RoutedEventArgs e)
        {
            client.clearData();
            client.askServerForUpdate();
        }

        //metoda buttona do zapisywania settingsów
        public void saveIDSettings(object sender, RoutedEventArgs e)
        {
            settingsPanel.Visibility = Visibility.Hidden;
            showSettingsPanel(false);
            client.buildJson();
        }

        //metoda buttona do anulowania settingsów
        public void cancelIDSettings(object sender, RoutedEventArgs e)
        {
            settingsPanel.Visibility = Visibility.Hidden;
            showSettingsPanel(false);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            client.buildJson();
        }

        private void optionButtonsPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            optionButtonsPanel.Opacity = 1;
        }

        private void optionButtonsPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            optionButtonsPanel.Opacity = 0;
        }

        private void onClick_FullScreenButton(object sender, RoutedEventArgs e)
        {
            isFullScreen = !isFullScreen;
        }

        private void onClick_SettingsButton(object sender, RoutedEventArgs e)
        {
            isSettingsPanelOpened = !isSettingsPanelOpened;
        }

        private void onClick_PauseButton(object sender, RoutedEventArgs e)
        {
            isPaused = !isPaused;
            client.shouldPause(isPaused);
        }

        private void checkboxFullscreen_Click(object sender, RoutedEventArgs e)
        {
            bool? result = checkboxFullscreen.IsChecked;
            if (result == true)
                client.fullscreen = true;
            else if (result == false)
                client.fullscreen = false;
        }

        private void NewFullScreenColor(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            var bc = new BrushConverter();
            fullBG = (Brush)bc.ConvertFromString(e.NewValue.ToString());
            refreshBG();
        }

        private void NewWindowScreenColor(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            var bc = new BrushConverter();
            windowBG = (Brush)bc.ConvertFromString(e.NewValue.ToString());
            refreshBG();
        }

        private void refreshBG()
        {
            if (isFullScreen)
            {
                Background = fullBG;
            }
            else
            {
                Background = windowBG;
            }
            settingsPanel.Background = Background;
        }

        private void resetIDEndpoint(object sender, RoutedEventArgs e)
        {
            client.clearEverything();
            client.reconnect();
        }
    }
}
