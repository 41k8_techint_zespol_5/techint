﻿using ClientApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp.Backend
{
    public static class JsonHandler
    {
        public static void saveClientLocaly(JsonSerializationModel model)
        {
            string json = JsonConvert.SerializeObject(model, Formatting.Indented);
            
            string dir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = dir + "/data.json";
            System.IO.File.WriteAllText(path, json);
            Console.WriteLine("saved json");
        }

        public static JsonSerializationModel readClientLocaly()
        {
            string dir = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = dir + "/data.json";

            if (System.IO.File.Exists(path))
            {
                JsonSerializationModel myModel = JsonConvert.DeserializeObject<JsonSerializationModel>(System.IO.File.ReadAllText(path));
                return myModel;
            }
            return null;
        }

        public static bool isEquall<T>(T lhs, T rhs)
        {
            string firstJson = JsonConvert.SerializeObject(lhs, Formatting.Indented);
            string secondJson = JsonConvert.SerializeObject(rhs, Formatting.Indented);

            if (firstJson == secondJson)
            {
                return true;
            }
            return false;
        }
    }
}
