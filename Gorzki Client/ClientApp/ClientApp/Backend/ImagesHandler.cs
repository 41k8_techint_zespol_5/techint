﻿using ClientApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ClientApp.Backend
{
    public static class ImagesHandler
    {
        private static List<string> mediaMap = new List<string>();
        public static void downloadImagesAndSetLocalPath(ref List<Scheduler> model)
        {
            mediaMap.Clear();
            foreach (Scheduler scheduler in model)
            {
                if (scheduler != null)
                {
                    foreach (Playlist playList in scheduler.Playlists)
                    {
                        if (playList != null)
                        {
                            foreach (MediaPlaylist mp in playList.MediaPlaylist)
                            {
                                if (mp != null)
                                {
                                    if (checkIfImageAlreadyExists(mp.Media.URL) == false)
                                    {
                                        string localPath = downloadImageAndReturnLocalPath(mp.Media.URL);
                                        mp.Media.localURL = localPath;
                                        mediaMap.Add(localPath);
                                    }
                                    else
                                    {
                                        mp.Media.localURL = getLocalPath(mp.Media.URL);
                                        mediaMap.Add(mp.Media.localURL);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            clearUnusedImages(ref mediaMap);
        }

        public static void clearUnusedImages(ref List<string> usedMedia)
        {
            string [] files = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "Images");

            foreach(string filePath in files)
            {
                try
                {
                    if (usedMedia.Exists(element => element.CompareTo(filePath) == 0) == false)
                    {
                        try
                        {
                            File.Delete(filePath);
                        }
                        catch (System.IO.IOException e)
                        {
                        }
                    }
                }
                catch (System.NullReferenceException e) { Console.WriteLine("Null reference exception przy clearowaniu"); }
            }
        }
        public static void removeImages()
        {
            string[] files = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "Images");

            foreach (string filePath in files)
            {
                try
                {
                    File.Delete(filePath);
                }
                catch (System.IO.IOException e)
                {
                }
            }
        }

        public static string downloadImageAndReturnLocalPath(string url)
        {
            string localImagePath = "";
            try
            {
                string lastPartOfPath = url.ToString().Split('/').Last();
                string pathEnd = lastPartOfPath.ToString().Split('?').First();
                string currentPath = AppDomain.CurrentDomain.BaseDirectory + pathEnd;

                WebClient webClient = new WebClient();
                webClient.DownloadFile(url, pathEnd);

                localImagePath = getLocalPath(url);

                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Images/") == false)
                {
                    DirectoryInfo di = Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Images/");
                }
                File.Move(new System.Uri(currentPath).LocalPath.ToString(), localImagePath);
            }
            catch (System.Net.WebException)
            {
                return null;
            }
            return localImagePath;
        }

        public static bool checkIfImageAlreadyExists(string url)
        {
            if (File.Exists(getLocalPath(url)))
                return true;
            return false;
        }

        public static string getLocalPath(string url)
        {
            string lastPart = url.ToString().Split('/').Last();
            string pathEnd = lastPart.ToString().Split('?').First();
            string currentImagesPath = AppDomain.CurrentDomain.BaseDirectory + "Images/" + pathEnd;
            string localImagePath = new System.Uri(currentImagesPath).LocalPath.ToString();
            return localImagePath;
        }
    }
}
