﻿using ClientApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp.Backend
{

    public class ActiveScheduler
    {
        private int currentPlaylistIndex = -1;
        private int currentPlaylistsCount = -1;
        private int currentMediaPlaylistIndex = -1;
        private int[] allMediaPlaylistsCount = { -1 };

        private Scheduler _currentScheduler;
        public Scheduler currentScheduler
        {
            get
            {
                return _currentScheduler;
            }
            set
            {
                _currentScheduler = value;
                if (_currentScheduler.Playlists != null)
                {
                    currentPlaylistsCount = _currentScheduler.Playlists.Count();
                    allMediaPlaylistsCount = getAllPlaylistsCount();
                    currentPlaylistIndex = 0;
                    currentMediaPlaylistIndex = 0;
                }
                else
                {
                    currentPlaylistsCount = -1;
                }
            }
        }

        public int[] getAllPlaylistsCount()
        {
            if(currentPlaylistsCount == 0)
                return new int[1]{-1};
            int[] counter = new int[currentPlaylistsCount];
            int iter = 0;

            foreach (Playlist playlist in _currentScheduler.Playlists)
            {
                if (playlist.MediaPlaylist == null)
                    counter[iter] = -1;
                else
                    counter[iter] = playlist.MediaPlaylist.Count();
                ++iter;
            }
            return counter;
        }

        public MediaPlaylist getCurrentMediaAndIncrementToNext()
        {

            if (currentPlaylistsCount >= 0 && allMediaPlaylistsCount[currentPlaylistIndex] >= 0)
            {
                MediaPlaylist mediaToReturn = _currentScheduler.Playlists.ElementAt(currentPlaylistIndex).MediaPlaylist.ElementAt(currentMediaPlaylistIndex);
                increment();
                return mediaToReturn;
            }
            return null;
        }
        private void increment()
        {
            if (currentMediaPlaylistIndex < allMediaPlaylistsCount[currentPlaylistIndex] - 1)
            {
                currentMediaPlaylistIndex++;
            }
            else
            {
                if (currentPlaylistIndex < currentPlaylistsCount - 1)
                {
                    currentPlaylistIndex++;
                    currentMediaPlaylistIndex = 0;
                }
                else
                {
                    currentPlaylistIndex = 0;
                    currentMediaPlaylistIndex = 0;
                }
            }
        }
    }
}
