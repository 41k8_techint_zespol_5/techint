﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using System.ComponentModel;
using System.Net;
using Newtonsoft.Json;
using ClientApp.Models;
using ClientApp.Backend;
using System.Timers;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows;
using System.IO;

namespace ClientApp
{
    class Client : INotifyPropertyChanged
    {
        HubConnection connection;
        IHubProxy proxy;
        List<Scheduler> schedulers;
        ActiveScheduler activeScheduler;
        MediaType currentTypeOfImage { get; set; }
        MediaElement mediaElementControl;
        Image imageControl;

        private Timer changeMediaTimer;
        private Timer changeSchedulerTimer;
        public string connectionID { get; private set; }
        public string clientName { get; private set; }
        public int? uniqueID { get; private set; }
        public string defaultImagePath { get; set; }

        private string _currentImagePath;
        public string currentImagePath
        {
            get
            {
                return _currentImagePath;
            }

            set
            {
                if (_currentImagePath == value) return;

                _currentImagePath = value;
                OnPropertyChanged("currentImagePath");
            }
        }

        public bool fullscreen { get; set; }
        private string _status;
        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                if (_status == value) return;

                _status = value;
                OnPropertyChanged("Status");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public Client()
        {
            clientName = "unnamed";
            Status = "unknown";
            uniqueID = null;
            schedulers = new List<Scheduler>();
            activeScheduler = new ActiveScheduler();
            defaultImagePath = AppDomain.CurrentDomain.BaseDirectory + @"\Default.jpg";
            currentImagePath = defaultImagePath;
        }

        public void startUp()
        {
            connection = new HubConnection("http://localhost:56733");
            proxy = connection.CreateHubProxy("EndpointsHub");
            mapFunctionsToBeUsedByServer();
            connection.Start();

            Scheduler scheduler = lookForActiveSchedulerInModel(ref schedulers);
            if (JsonHandler.isEquall<Scheduler>(activeScheduler.currentScheduler, scheduler) == false)
            {
                activeScheduler.currentScheduler = scheduler;
            }

            initChangeMediaTimer();
            initChangeSchedulerTimer();

            connection.Closed += WhenDisconnected;
            connection.StateChanged += OnStateChanged;
        }

        public void initChangeMediaTimer()
        {
            changeMediaTimer = new System.Timers.Timer();
            changeMediaTimer.Interval = 5000;
            changeMediaTimer.Elapsed += changeMediaEvent;
            changeMediaTimer.Enabled = true;
        }

        public void initChangeSchedulerTimer()
        {
            changeSchedulerTimer = new System.Timers.Timer();
            changeSchedulerTimer.Interval = 30000;
            changeSchedulerTimer.Elapsed += changeSchedulerEvent;
            changeSchedulerTimer.Enabled = true;
        }
        private void changeSchedulerEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("change schedular - The Elapsed event was raised at {0}", e.SignalTime);
            if (checkIfSchedulerCouldBeActive(activeScheduler.currentScheduler) == false)
            {
                Scheduler scheduler = lookForActiveSchedulerInModel(ref schedulers);
                if (JsonHandler.isEquall<Scheduler>(activeScheduler.currentScheduler, scheduler) == false)
                {
                    activeScheduler.currentScheduler = scheduler;
                }
            }
        }

        private void changeMediaEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("change media -The Elapsed event was raised at {0}", e.SignalTime);

            if (activeScheduler.currentScheduler.ID == 0)
            {
                currentImagePath = defaultImagePath;
                Application.Current.Dispatcher.Invoke(() =>
                 {
                     imageControl.Visibility = System.Windows.Visibility.Visible;
                     mediaElementControl.Visibility = System.Windows.Visibility.Hidden;
                 });
            }
            else
            {
                MediaPlaylist mediaPlaylist = activeScheduler.getCurrentMediaAndIncrementToNext();

                if (mediaPlaylist != null)
                {
                    if (mediaPlaylist.playTime < 1)
                    {
                        changeMediaTimer.Interval = 1000;
                    }
                    else
                    {
                        changeMediaTimer.Interval = mediaPlaylist.playTime * 1000;
                    }
                    if (mediaPlaylist.Media.localURL != null)
                    { 
                        if (mediaPlaylist.Media.Type == MediaType.PHOTO)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                imageControl.Visibility = System.Windows.Visibility.Visible;
                                mediaElementControl.Visibility = System.Windows.Visibility.Hidden;
                            });
                        }
                        else if (mediaPlaylist.Media.Type == MediaType.MOVIE)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                imageControl.Visibility = System.Windows.Visibility.Hidden;
                                mediaElementControl.Visibility = System.Windows.Visibility.Visible;
                            });
                        }
                        currentImagePath = mediaPlaylist.Media.localURL;
                    }
                }
            }
        }

        private void OnStateChanged(StateChange obj)
        {
            Status = obj.NewState.ToString();
            if (obj.NewState == ConnectionState.Connected)
            {
                connectionID = connection.ConnectionId;
                OnPropertyChanged("clientName");
                OnPropertyChanged("uniqueID");
                OnPropertyChanged("connectionID");
                askServerForUpdate();
            }
        }

        private void WhenDisconnected()
        {

            Console.WriteLine("Connection closed. Atempting to reconnect...");
            connection.Closed -= WhenDisconnected;
            connection.StateChanged -= OnStateChanged;

            connection = new HubConnection("http://localhost:56733");
            proxy = connection.CreateHubProxy("EndpointsHub");
            mapFunctionsToBeUsedByServer();
            connection.StateChanged += OnStateChanged;
            var t = connection.Start();

            bool result = false;
            t.ContinueWith(task =>
            {
                if (!task.IsFaulted)
                {
                    result = true;
                    Console.WriteLine("Connection estabilished.");
                }
            }).Wait(5000);

            if (!result)
            {
                WhenDisconnected();
            }
        }

        public void buildJson()
        {
            JsonSerializationModel model = new JsonSerializationModel
            {
                clientName = this.clientName,
                uniqueID = this.uniqueID,
                schedulers = this.schedulers,
                defaultImagePath = this.defaultImagePath,
                currentTypeOfImage = this.currentTypeOfImage,
                fullScreen = this.fullscreen
            };
            JsonHandler.saveClientLocaly(model);
        }

        public void readJsonModel()
        {
            JsonSerializationModel model = JsonHandler.readClientLocaly();
            if (model != null)
            {
                this.clientName = model.clientName;
                this.uniqueID = model.uniqueID;
                this.schedulers = model.schedulers;
                this.defaultImagePath = model.defaultImagePath;
                this.currentTypeOfImage = model.currentTypeOfImage;
                this.fullscreen = model.fullScreen;
            }
        }

        public void saveImagesFromModel()
        {
            ImagesHandler.downloadImagesAndSetLocalPath(ref schedulers);
        }

        public Scheduler lookForActiveSchedulerInModel(ref List<Scheduler> schedulers)
        {
            foreach (Scheduler scheduler in schedulers)
            {
                if (checkIfSchedulerCouldBeActive(scheduler) == true)
                {
                    Console.WriteLine("Zmiana schedulera");
                    return scheduler;
                }
            }
            return new Scheduler();
        }

        private bool checkIfSchedulerCouldBeActive(Scheduler scheduler)
        {
            DateTime currentDate = DateTime.Now;
            if ((currentDate.CompareTo(scheduler.StartTime) >= 0) && (currentDate.CompareTo(scheduler.EndTime) < 0))
            {
                return true;
            }
            return false;
        }

        private void mapFunctionsToBeUsedByServer()
        {
            mapFunctionChangeEndpointName();
            mapFunctionAssignUniqueId();
            mapFunctionIntroduceYourself();
            mapFunctionSendSchedule();
            mapFunctionChangeDefaultImage();
        }

        internal void setImagesHandler(ref System.Windows.Controls.MediaElement mediaElement, ref System.Windows.Controls.Image image)
        {
            this.mediaElementControl = mediaElement;
            this.imageControl = image;
        }

        public void clearData()
        {
            try
            {
                currentImagePath = defaultImagePath; 
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "data.json");
                ImagesHandler.removeImages();
                schedulers.Clear();
            }
            catch(Exception e)
            {

            }
        }

        public void clearEverything()
        {
            clientName = "unnamed";
            uniqueID = null;
            clearData();
        }

        public void shouldPause(bool value)
        {
            if (value == true)
                changeMediaTimer.Stop();
            else
                changeMediaTimer.Start();
        }

        private void mapFunctionChangeEndpointName()
        {
            proxy.On<string>("ChangeEndpointName", newName =>
            {
                Console.WriteLine("Old Client name {0}  - New client name {1}", clientName, newName);
                OnPropertyChanged("clientName");
                this.clientName = newName;
                this.buildJson();
            });
        }

        private void mapFunctionChangeDefaultImage()
        {
            proxy.On<string>("ChangeDefaultImage", imageURL =>
            {
                string localPath = ImagesHandler.downloadImageAndReturnLocalPath(imageURL);
                if (localPath != null)
                    this.defaultImagePath = localPath;
            });
        }

        private void mapFunctionIntroduceYourself()
        {
            proxy.On("IntroduceYourself", () =>
            {
                if (connection.State == ConnectionState.Connected)
                {
                    object id = uniqueID;
                    proxy.Invoke("IntroduceMyself", id, connection.ConnectionId);
                }
            });
        }

        private void mapFunctionAssignUniqueId()
        {
            proxy.On<int>("AssignUniqueId", uniqueID =>
            {
                this.uniqueID = uniqueID;
                OnPropertyChanged("uniqueID");
                Console.WriteLine("PRZYPISANO UNIQUE ID");
            });
        }

        private void mapFunctionSendSchedule()
        {
            proxy.On<EndpointDataExchange>("SendSchedulers", endpointDataExchange =>
            {
                schedulers.Clear();
                schedulers = endpointDataExchange.Schedulers.ToList();
                clientName = endpointDataExchange.endpointName;
                Console.WriteLine("ODEBRANO SCHEDULE");
                ImagesHandler.downloadImagesAndSetLocalPath(ref schedulers);
                this.buildJson();
                Scheduler scheduler = lookForActiveSchedulerInModel(ref schedulers);
                if (JsonHandler.isEquall<Scheduler>(activeScheduler.currentScheduler, scheduler) == false)
                {
                    activeScheduler.currentScheduler = scheduler;
                }
            });
        }

        /// <summary>
        /// Invoke on server
        /// </summary>
        public bool ChangeNameOnServer(string newName)
        {
            if (connection.State == ConnectionState.Connected)
            {
                object id = uniqueID;
                proxy.Invoke("ChangeMyName", id, newName);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Invoke on server
        /// </summary>
        public void askServerForUpdate()
        {
            if (connection.State == ConnectionState.Connected)
            {
                object id = uniqueID;
                proxy.Invoke("UpdateMyData",id);
            }
        }

        public void reconnect()
        {
            connection.Stop();
            connection.Start();
        }
    }
}
