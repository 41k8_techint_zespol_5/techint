﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientApp.Models
{
    public class MediaPlaylist
    {
        public int ID { get; set; }
        public int playTime { get; set; }
        public virtual Media Media { get; set; }
    }
}