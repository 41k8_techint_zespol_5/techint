﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientApp.Models
{
    public class EndpointDataExchange
    {
        public ICollection<Scheduler> Schedulers { get; set; }
        public string endpointName { get; set; }
    }
}