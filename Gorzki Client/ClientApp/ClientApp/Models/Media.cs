﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientApp.Models
{
    public enum MediaType
    {
        MOVIE, PHOTO
    }

    public class Media
    {
        public int ID { get; set; }
        public string FileName { get; set; }
        public string URL { get; set; }
        public string localURL { get; set; }
        public int Duration { get; private set; }
        public MediaType Type { get; set; }
    }
}