﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientApp.Models
{
    public class Playlist
    {
        public int ID { get; set; }
        public string playlistName { get; set; }
        virtual public ICollection<MediaPlaylist> MediaPlaylist { get; set; }
        [JsonIgnore]
        public virtual ICollection<Scheduler> Schedulers { get; set; }

    }
}