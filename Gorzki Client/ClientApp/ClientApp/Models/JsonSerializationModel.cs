﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp.Models
{
    public class JsonSerializationModel
    {
        public List<Scheduler> schedulers { get; set; }
        public string clientName { get; set; }
        public int? uniqueID { get; set; }
        public string defaultImagePath { get; set; }
        public MediaType currentTypeOfImage { get; set; }
        public bool fullScreen { get; set; }
    }
}
