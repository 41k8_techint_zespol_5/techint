﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp
{
    public partial class FirstLoginPopup : Window
    {
        public bool firstTimeLoginBool = true;
        // ^ ten bool miał decydować o lostach wszystkiego, ale aktualnie został jako wspomnienie idei


        public Window mainWindow;
        Client client;
        public FirstLoginPopup()
        {
            client = new Client();
            
            if (client.uniqueID != null/*firstTimeLoginBool == false*/)
                InitializeComponent();
            else
            {
                mainWindow = new MainWindow();
                mainWindow.Activate();
                mainWindow.Show();
                this.Close();
            }
        }

        //wprowadź ręcznie ID
        public void insertHandID(object sender, RoutedEventArgs e)
        {
            //tutaj pobierz sobie do jakiś zmiennyych istotne value
            mainWindow = new MainWindow();
            mainWindow.Activate();
            mainWindow.Show();
            firstTimeLoginBool = false;
            this.Close();      
        }

        //pobierz ID z serwera
        public void uploadIDFromServer(object sender, RoutedEventArgs e)
        {
            // to samo, pobierz id z serwera, przekaż do zmiennych w mainwindow.xaml.cs
            mainWindow = new MainWindow();
            mainWindow.Activate();
            mainWindow.Show();
            firstTimeLoginBool = false;
            this.Close();
        }
    }
}
