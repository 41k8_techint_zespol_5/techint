﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServerMVCApplication.Models;
using System.IO;
using System.Diagnostics;
using System.Web.Hosting;
using System.Text.RegularExpressions;
using ServerMVCApplication.DataAccessLayer;
using ServerMVCApplication.Backend;

namespace ServerMVCApplication.Controllers
{
    public class HomeController : Controller
    {
        private SystemContext db = new SystemContext();
        public ActionResult Index()
        {
            return View(db.Endpoints.ToList());
        }

        public List<string> getAllFileNames()
        {
            // Temporary to be not deleted, mayby usefull in future
            List<string> fileNames = new List<string>();
            string[] dirs = Directory.GetFiles(Server.MapPath("~/Images"));
            foreach (string dir in dirs)
            {
                String[] substrings = dir.Split('\\');
                Debug.WriteLine(substrings.Last());
                fileNames.Add(substrings.Last());
            }
            return fileNames;
        }
    }
}