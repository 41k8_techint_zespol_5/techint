﻿using ServerMVCApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.ModelBinding;

namespace ServerMVCApplication.Controllers
{
    public class EndPointsController : Controller
    {
        // GET: EndPoint
        public ActionResult Index()
        {
            return View();
        }

        // Metoda do pobrania wszystkich podpiętych końcówek
        [HttpGet]
        public ActionResult getAll()
        {
            return Json(UserHandler.getUserList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetOne(int id)
        {
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        /*[HttpPost]
        public ActionResult setDefault(int id)
        {
            System.Diagnostics.Debug.WriteLine("ALIVE!");
            string dupa = MyHttpRequestHelper.GetFromBodyString(Request);
            System.Diagnostics.Debug.WriteLine(dupa);
            return Json(dupa, JsonRequestBehavior.AllowGet);
        }*/

        // setDefault/:id - ustawia domyślne media dla końcówki o :id { media: id_mediow }
        [HttpPost]
        public ActionResult setDefault(int id)
        {

            return Json(id, JsonRequestBehavior.AllowGet);
        }

        // rename/:id - zmienia nazwę końcówki o id z url na podaną w data
        [HttpPost]
        public ActionResult rename(int id)
        {

            return Json(id, JsonRequestBehavior.AllowGet);
        }

        // setGroup/:id - przypisuje końcówkę do grupy :id
        [HttpPost]
        public ActionResult setGroup(int id)
        {

            return Json(id, JsonRequestBehavior.AllowGet);
        }

        // setSchedule/:id - ustawia skedżul końcówki o :id { dateStart: date, dateEnd: date, media: id_mediów }
        [HttpPost]
        public ActionResult setSchedule(int id)
        {

            return Json(id, JsonRequestBehavior.AllowGet);
        }


    }
}