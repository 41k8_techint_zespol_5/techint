﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ServerMVCApplication.DataAccessLayer;
using ServerMVCApplication.Models;

namespace ServerMVCApplication.Controllers
{
    public class EndpointsController : Controller
    {
        private SystemContext db = new SystemContext();

        // GET: Endpoints
        public ActionResult Index()
        {
            return View(db.Endpoints.ToList());
        }

        // GET: Endpoints/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Endpoint endpoint = db.Endpoints.Find(id);
            if (endpoint == null)
            {
                return HttpNotFound();
            }
            return View(endpoint);
        }

        // GET: Endpoints/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Endpoints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,GroupID,ConnectionID")] Endpoint endpoint)
        {
            if (ModelState.IsValid)
            {
                db.Endpoints.Add(endpoint);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(endpoint);
        }

        // GET: Endpoints/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Endpoint endpoint = db.Endpoints.Find(id);
            if (endpoint == null)
            {
                return HttpNotFound();
            }
            return View(endpoint);
        }

        // POST: Endpoints/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,GroupID,ConnectionID")] Endpoint endpoint)
        {
            if (ModelState.IsValid)
            {
                db.Entry(endpoint).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(endpoint);
        }

        // GET: Endpoints/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Endpoint endpoint = db.Endpoints.Find(id);
            if (endpoint == null)
            {
                return HttpNotFound();
            }
            return View(endpoint);
        }

        // POST: Endpoints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Endpoint endpoint = db.Endpoints.Find(id);
            db.Endpoints.Remove(endpoint);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
