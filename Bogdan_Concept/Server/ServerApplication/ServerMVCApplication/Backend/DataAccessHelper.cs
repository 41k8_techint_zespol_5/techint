﻿using ServerMVCApplication.DataAccessLayer;
using ServerMVCApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Backend
{
    public class DataAccessHelper
    {
        private SystemContext db = new SystemContext();

        public bool checkIfEndPointExists(int id)
        {
            Endpoint endpoint = db.Endpoints.Find(id);
            if (endpoint == null)
            {
                return false;
            }
            return true;
        }

        public Endpoint getEndPoint(int id)
        {
            Endpoint endpoint = db.Endpoints.Find(id);
            return endpoint;
        }
        public void addNewEndPoint(Endpoint newEndPoint)
        {
            db.Endpoints.Add(newEndPoint);
            db.SaveChanges();
        }
        public void removeEndPoint(int id)
        {
            if(checkIfEndPointExists(id) == true)
            {
                Endpoint endpoint = db.Endpoints.Find(id);
                db.Endpoints.Remove(endpoint);
                db.SaveChanges();
            }
        }
        public void editEndPoint(Endpoint endpoint)
        {
            db.Entry(endpoint).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void changeEndPointName(int id, string name)
        {
            Endpoint endpoint = getEndPoint(id);
            endpoint.Name = name;
            editEndPoint(endpoint);
        }
        public void changeEndPointGroupId(int id, int groupID)
        {
            Endpoint endpoint = getEndPoint(id);
            endpoint.GroupID = groupID;
            editEndPoint(endpoint);
        }
        public void changeEndPointConnectionId(int id, string connectionID)
        {
            System.Diagnostics.Debug.WriteLine("DEBUGED");
            Endpoint endpoint = getEndPoint(id);
            endpoint.ConnectionID = connectionID;
            editEndPoint(endpoint);
            System.Diagnostics.Debug.WriteLine("WPISANO {0}  {1} ", endpoint.ID, connectionID);
        }
        public int getIdOfLastEndPoint()
        {
            int lastID = db.Endpoints.Max(end => end.ID);
            return lastID;
        }

        public Endpoint getLastEndPoint()
        {
            Endpoint endpoint = db.Endpoints
                                  .OrderByDescending(p => p.ID)
                                  .FirstOrDefault();
            return endpoint;
        }


        public void createNewEndPoint(string connectionID)
        {
            Endpoint endpoint = new Endpoint();
            endpoint.Name = "unnamed";
            endpoint.ID = 0;
            endpoint.ConnectionID = connectionID;
            endpoint.GroupID = 1;
            addNewEndPoint(endpoint);
        }


    }
}