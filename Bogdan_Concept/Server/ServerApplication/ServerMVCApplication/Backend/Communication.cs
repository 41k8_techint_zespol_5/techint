﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using ServerMVCApplication.DataAccessLayer;
using ServerMVCApplication.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;


namespace ServerMVCApplication.Backend
{
    public class CommunicationHub : Hub
    {
        public void SendToAll(string message)
        {
            Clients.All.sendUriPathToClient(message);
        }

        public void SendToUserWithSpecificId(string [] connectionIds, string message)
        {
            foreach (String connectionId in connectionIds)
            {
                if (connectionId != null && connectionId != "")
                {
                    Clients.Client(connectionId).sendUriPathToClient(message);
                }
            }
        }

        public override Task OnConnected()
        {
            Clients.Client(Context.ConnectionId).introduceYourself();
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }

        public void server_introdcution(object id, string connectionId)
        {
            DataAccessHelper dataAccessHelper = new DataAccessHelper();
            bool result = false;
            if (id != null)
                result = dataAccessHelper.checkIfEndPointExists(Convert.ToInt32(id));
            if(result == true)
            {
                dataAccessHelper.changeEndPointConnectionId(Convert.ToInt32(id), connectionId);
            }
            else
            {
                dataAccessHelper.createNewEndPoint(connectionId);
                int endPointUniqueId = dataAccessHelper.getIdOfLastEndPoint();
                Clients.Client(connectionId).assignUniqueId(endPointUniqueId);
            }
        }

        public void updateData()
        {
            SystemContext db = new SystemContext();
            EndpointDataExchange endpointDataExchange = new EndpointDataExchange();
            endpointDataExchange.Schedulers = new List<Scheduler>();

            endpointDataExchange.Schedulers.Add(db.Schedulers.Find(1));
            endpointDataExchange.Schedulers.Add(db.Schedulers.Find(2));

            Clients.All.sendSchedule(endpointDataExchange);
        }
    }
}