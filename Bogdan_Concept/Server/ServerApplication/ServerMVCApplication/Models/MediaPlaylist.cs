﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Models
{
    public class MediaPlaylist
    {
        public int ID { get; set; }
        public int PlaylistID { get; set; }
        public int MediaID { get; set; }
        public int playTime { get; set; }
        public virtual Media Media { get; set; }
    }
}