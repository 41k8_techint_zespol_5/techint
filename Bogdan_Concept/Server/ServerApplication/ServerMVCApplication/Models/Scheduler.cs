﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Models
{
    public class Scheduler
    {
        public int ID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public virtual ICollection<Playlist> Playlists { get; set; }
    }
}