﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Models
{
    public class Playlist
    {
        public int ID { get; set; }
        public string playlistName { get; set; }
        public virtual IDictionary<int, int> MediaTimeDictionary { get; set; }
    }
}