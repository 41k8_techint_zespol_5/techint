﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Models
{
    public class Endpoint
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int GroupID { get; set; }
        public string ConnectionID { get; set; }
        public virtual ICollection<Scheduler> Schedulers { get; set; }
    }
}