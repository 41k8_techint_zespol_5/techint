﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Models
{
    public class Group
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Endpoint> Endpoints { get; set; }
        public virtual ICollection<Scheduler> Schedulers { get; set; }

    }
}