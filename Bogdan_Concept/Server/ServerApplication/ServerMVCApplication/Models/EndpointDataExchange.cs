﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Models
{
    public class EndpointDataExchange
    {
        public ICollection<Scheduler> Schedulers { get; set; }
    }
}