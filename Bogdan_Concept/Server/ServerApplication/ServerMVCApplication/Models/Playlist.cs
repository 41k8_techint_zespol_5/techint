﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServerMVCApplication.Models
{
    public class Playlist
    {
        public int ID { get; set; }
        public string playlistName { get; set; }
        virtual public ICollection<MediaPlaylist> mediaPlaylist { get; set; }

    }
}