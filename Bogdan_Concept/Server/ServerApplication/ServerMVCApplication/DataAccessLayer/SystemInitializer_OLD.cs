﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ServerMVCApplication.Models;

namespace ServerMVCApplication.DataAccessLayer
{
    public class SystemInitializer : System.Data.Entity. DropCreateDatabaseIfModelChanges<SystemContext>
    {
        protected override void Seed(SystemContext context)
        {
            var groups = new List<Group>
            {
                new Group { Name ="Default" }
            };

            groups.ForEach(g => context.Groups.Add(g));
            context.SaveChanges();

        }
    }
}