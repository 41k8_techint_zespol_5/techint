﻿using ServerMVCApplication.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ServerMVCApplication.DataAccessLayer
{
    public class SystemContext : DbContext
    {
        public SystemContext(): base("SystemContext")
        {
        }

        public DbSet<Endpoint> Endpoints { get; set;}
        public DbSet<Group> Groups { get; set; }
        public DbSet<Scheduler> Schedulers { get; set; }
        public DbSet<Playlist> Playlists { get; set; }
        public DbSet<Media> Medias { get; set; }
        public DbSet<MediaPlaylist> MediaPlaylist { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}