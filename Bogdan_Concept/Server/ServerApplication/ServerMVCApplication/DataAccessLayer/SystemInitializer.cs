﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ServerMVCApplication.Models;

namespace ServerMVCApplication.DataAccessLayer
{
    public class SystemInitializer : System.Data.Entity.DropCreateDatabaseAlways<SystemContext>//DropCreateDatabaseIfModelChanges<SystemContext>
    {
        protected override void Seed(SystemContext context)
        {
            var groups = new List<Group>
            {
                new Group { Name ="Default" }
            };

            groups.ForEach(g => context.Groups.Add(g));
            context.SaveChanges();

            var medias = new List<Media>
            {
                new Media { FileName="aston.rhe", Type=MediaType.PHOTO, URL="http://media.caranddriver.com/images/media/51/12-aston-martin-db11-843-photo-667946-s-original.jpg" },
                new Media { FileName="buszus", Type=MediaType.PHOTO, URL="http://wallup.net/wp-content/uploads/2016/01/25690-StarCraft-James_Raynor-astronaut-space-cat-Starcraft_II-humor-736x459.jpg"},
                new Media { FileName="NyanCat", Type=MediaType.PHOTO, URL="https://tctechcrunch2011.files.wordpress.com/2015/08/safe_image.gif"},
                new Media { FileName="Cat1", Type=MediaType.PHOTO, URL="https://yt3.ggpht.com/-V92UP8yaNyQ/AAAAAAAAAAI/AAAAAAAAAAA/zOYDMx8Qk3c/s900-c-k-no-mo-rj-c0xffffff/photo.jpg"},
                new Media { FileName="Cat2", Type=MediaType.PHOTO, URL="http://i3.mirror.co.uk/incoming/article6745694.ece/ALTERNATES/s615b/Ginger-tabby-cat.jpg"},
                new Media { FileName="Cat3", Type=MediaType.PHOTO, URL="http://www.catprotection.com.au/wp-content/uploads/2014/11/5507692-cat-m.jpg"},
                new Media { FileName="Cat4", Type=MediaType.PHOTO, URL="https://www.vetprofessionals.com/catprofessional/images/home-cat.jpg"},
                new Media { FileName="Cat5", Type=MediaType.PHOTO, URL="http://hollyandhugo.com/blog/wp-content/uploads/2015/10/Cat-Care11.jpg"},
                new Media { FileName="Cat6", Type=MediaType.PHOTO, URL="http://www.lifewithcats.tv/wp-content/uploads/2012/10/cat-linncurrie.jpg"},
                new Media { FileName="Cat7", Type=MediaType.PHOTO, URL="https://3b0ad08da0832cf37cf5-435f6e4c96078b01f281ebf62986b022.ssl.cf3.rackcdn.com/articles/content/How-old-is-my-cat-in-human-years_0af22fe2-0f4a-4b24-821f-695be0d18cee.jpg"},
                new Media { FileName="Cat8", Type=MediaType.PHOTO, URL="http://www.catgifpage.com/gifs/310.gif"},
                new Media { FileName="Cat9", Type=MediaType.PHOTO, URL="https://cdn0.vox-cdn.com/thumbor/XjfWvizyYYXbALK9tTW8mzLJMMs=/1x0:369x207/1600x900/cdn0.vox-cdn.com/uploads/chorus_image/image/32606283/q3e87zR.0.gif"},
                new Media { FileName="Crab1", Type=MediaType.PHOTO, URL="https://s-media-cache-ak0.pinimg.com/736x/1c/36/7d/1c367d0890e666e680d10ce58fd91cdc.jpg"},
                new Media { FileName="Crab2", Type=MediaType.PHOTO, URL="http://cdn.c.photoshelter.com/img-get/I00004wCco3TOC0s/s/750/750/DIM-Crab01.jpg"},
                new Media { FileName="Crab3", Type=MediaType.PHOTO, URL="http://i2.kym-cdn.com/photos/images/newsfeed/001/021/833/d2e.jpg"},
                new Media { FileName="Crab4", Type=MediaType.PHOTO, URL="http://i3.kym-cdn.com/photos/images/original/001/021/851/118.jpg"}
            };
            medias.ForEach(m => context.Medias.Add(m));
            context.SaveChanges();

            Playlist playList1 = new Playlist();
            playList1.playlistName = "Random";

            Playlist playList2 = new Playlist();
            playList2.playlistName = "KATZ";

            Playlist playList3 = new Playlist();
            playList3.playlistName = "CRABZ";

            var mediaPlayLists1 = new List<MediaPlaylist> { 
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 1), playTime = 7 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 2), playTime = 6 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 3), playTime = 4 }
            };

            var mediaPlayLists2 = new List<MediaPlaylist> { 
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 4), playTime = 5 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 5), playTime = 7 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 6), playTime = 11 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 7), playTime = 4 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 8), playTime = 7 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 9), playTime = 9 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 10), playTime = 3 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 11), playTime = 3 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 12), playTime = 3 }
            };

           var mediaPlayLists3 = new List<MediaPlaylist> { 
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 13), playTime = 5 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 14), playTime = 7 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 15), playTime = 11 },
                new MediaPlaylist { Media = context.Medias.Single(m => m.ID == 16), playTime = 4 }
            };


            playList1.mediaPlaylist = mediaPlayLists1;
            playList2.mediaPlaylist = mediaPlayLists2;
            playList3.mediaPlaylist = mediaPlayLists3;

            context.Playlists.Add(playList1);
            context.Playlists.Add(playList2);
            context.Playlists.Add(playList3);
            context.SaveChanges();

            var schedulers = new List<Scheduler>
            {
                new Scheduler { StartTime = DateTime.Now, EndTime = DateTime.Now.AddMinutes(20), Playlists = new List<Playlist> {playList1, playList2}},
                new Scheduler { StartTime = DateTime.Now.AddMinutes(15), EndTime = DateTime.Now.AddMinutes(35), Playlists = new List<Playlist> {playList3}}
            };

            schedulers.ForEach(s => context.Schedulers.Add(s));
            context.SaveChanges();
        }
    }
}